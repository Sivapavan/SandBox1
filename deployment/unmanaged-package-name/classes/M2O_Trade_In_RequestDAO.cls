/**
@Classname :       M2O_Trade_In_RequestDAO
@Description :     This class is written to query/DML operation on Trade-in Request,Broker Bids  
@Author:           Dipali Vijapure
@Last Modified:    18-June-2017
**/
public class M2O_Trade_In_RequestDAO {

 /**
    @Description: Update the Trade-In Request record into the database.
    @Param :      List of Trade-In request
    @Return:      Null
    **/
    
    public void updateTradeinRequest (List<Trade_In_Request__c> listTradeinReq) {
    
        try {
            if (listTradeinReq != null && !listTradeinReq .isEmpty()) {
            
                update listTradeinReq ;
            }
        }catch(DMLException ex) { system.debug('M2O_Trade_In_RequestDAO.Update failed ' + ex);}
    }

/**Description: Query Broker Bids
       Return: Broker Bids list
    **/
    public static List<Broker_Bids__c> getBrokerBids(Set<Id> TradeinRequestId) {
       List<Broker_Bids__c> brokerBidsList = new List<Broker_Bids__c>();
        try{
            if (TradeinRequestId != null && !TradeinRequestId.isEmpty()){
                brokerBidsList = [Select Id, Best_Bid__c, Type__c,Trade_In_Desk_Location__c, Status__c, Trade_In_Request__c,Broker__c,Bid_Price__c,Bid_Currency__c, Trade_In_Request__r.name,Broker__r.Email,Broker__r.Phone,Broker__r.name from Broker_Bids__c where Trade_In_Request__c IN :TradeInRequestId and Type__c = 'Bid'];
            }
            if(brokerBidsList != Null && !brokerBidsList.isEmpty()){
                return brokerBidsList;
            }
            return null;
        }catch(DMLException ex){system.debug('M2O_Trade_In_RequestDAO.getBrokerBids failed -----> '+ex.getmessage()); return null;}
    }
    
    /**Description: Query Trade-in Request
       Return: Trade In Request List
    **/
    public static List<Trade_In_Request__c> getTradeInRequest(String TIRNumber) {
       List<Trade_In_Request__c> TradeinList= new List<Trade_In_Request__c>();
        try{
            if (TIRNumber != null && TIRNumber!= ''){
                TradeinList = [Select Id,Modality__c,OEM_of_Trade_In_System__c,Key_Market__c,Trade_In_Desk_Location__c,Hospital_Customer_Name__c,New_Sale_Opportunity__c,Bid_Validity_Date__c,New_Sale_Opportunity__r.CurrencyIsoCode,Project_Manager_Email__c,De_installation_team_functional_email__c,Owner.Email,OwnerId from Trade_In_Request__c where Name =: TIRNumber];
            }
            if(TradeinList != Null && !TradeinList.isEmpty()){
                return TradeinList;
            }
            return null;
        }catch(DMLException ex){system.debug('M2O_Trade_In_RequestDAO.getTradeInRequest failed -----> '+ex.getmessage()); return null;}
    }
    /**Description: Query Contact
       Return: COntact list
    **/
    public static List<Contact> getContact(String EmailAddress ,  List<Trade_In_Request__c> TradeinList) {
        List<Contact> conList =  new List<Contact>();
        try{
            if (EmailAddress != null && EmailAddress != '' && TradeinList !=  null && !TradeinList.isEmpty()){
            conList = [select Id from contact where email =: EmailAddress and Inactive_Flag__c = true and ((Modality__c ='All' OR Modality__c includes( :TradeinList[0].Modality__c)) AND (OEM_of_Trade_In_System__c ='All' OR OEM_of_Trade_In_System__c includes (:TradeinList[0].OEM_of_Trade_In_System__c)) AND (Key_Market__c = 'All' OR Key_Market__c includes (:TradeinList[0].Key_Market__c))) limit 1];
            
            }
            if(conList != null && conList.size()>0){
                return conList;
            }
            return null;
        }catch(DMLException ex){ system.debug('M2O_Trade_In_RequestDAO.getContact failed -----> '+ex.getmessage()); return null; }
    }
    
      /**Description: Query Contact
       Return: COntact list
    **/
    public static List<Contact> getContactList(String EmailAddress ,  List<Trade_In_Request__c> TradeinList) {
           List<Contact> conList = new List<Contact>();
       // Contact con =  new Contact();
        try{
            if (EmailAddress != null && EmailAddress != '' && TradeinList !=  null && !TradeinList.isEmpty()){
            conList = [select Id from contact where email =: EmailAddress and Inactive_Flag__c = true and ((Modality__c ='All' OR Modality__c includes( :TradeinList[0].Modality__c)) AND (OEM_of_Trade_In_System__c ='All' OR OEM_of_Trade_In_System__c includes (:TradeinList[0].OEM_of_Trade_In_System__c)) AND (Key_Market__c = 'All' OR Key_Market__c includes (:TradeinList[0].Key_Market__c))) limit 1];
            
            }
            if(conList != Null && conList.size() > 0){
                return conList;
            }
            return null;
        }catch(DMLException ex){ system.debug('M2O_Trade_In_RequestDAO.getContact failed -----> '+ex.getmessage()); return null; }
    }
}